pipeline {
    agent {
        kubernetes {
            cloud 'shared-tools-k8s-cluster'
            containerTemplate {
                name 'jenkins-builder'
                image 'harbor.tkg.ictipzs.net/cicd/jenkins-buider:v1.2-jenkins'
                command 'sleep'
                args '99d'
            }
        }
    }

    environment {
        BUILDER = 'full-jammy'
        SERVICE_ACCOUNT = 'jenkins-cicd-tools-sa'
        APP_NAME='angular-demo-app'
        GIT_SOURCE_REPOSITORY='https://gitlab.com/sanagaraj-pivotal/angular-demo-app.git'
    }

    options {
        ansiColor('xterm')
        disableConcurrentBuilds()
        gitLabConnection('gitlab-auth')
    }

    stages {
        stage('kubernetes-login') {
           when {
               branch 'main'
           }
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins-ad-user', usernameVariable: 'USER', passwordVariable: 'KUBECTL_VSPHERE_PASSWORD')]) {
                  sh 'kubectl vsphere login --server=api.tkg.ictipzs.net  --insecure-skip-tls-verify --tanzu-kubernetes-cluster-name build-cluster --tanzu-kubernetes-cluster-namespace shared --vsphere-username $USER'
                }
            }
        }
        stage('Build image') {
            when {
                branch 'main'
            }
            steps {
              sh '''
              kp image save $APP_NAME-$GIT_COMMIT  \
              --tag harbor.tkg.ictipzs.net/meditheft/$APP_NAME:$GIT_COMMIT  \
              --git $GIT_SOURCE_REPOSITORY  \
              --git-revision $GIT_COMMIT  \
              --cluster-builder $BUILDER  \
              --service-account $SERVICE_ACCOUNT \
              --env BP_NODE_RUN_SCRIPTS=build \
              --env BP_WEB_SERVER=nginx \
              --env "BP_WEB_SERVER_ENABLE_PUSH_STATE=true" \
              --env "NODE_ENV=production" \
              --env "BP_WEB_SERVER_ROOT=dist/$APP_NAME" \
              --env "PORT=80"
              '''
              sh 'kp build logs $APP_NAME-$GIT_COMMIT'
              sh 'kp builds list $APP_NAME-$GIT_COMMIT | grep -q SUCCESS'
              script {
                  tmp_param =  sh (script: 'kubectl get image $APP_NAME-$GIT_COMMIT -o json | jq -r .status.latestImage', returnStdout: true).trim()
                  env.image = tmp_param
              }
            }
        }
    }
}
