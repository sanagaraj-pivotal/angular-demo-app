//AppConfig.d.ts
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

export interface AppConfig {
  appUrl: string;
}

//AppConfigService.ts
@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  private config: AppConfig = {appUrl : "hello"};
  loaded = false;
  constructor(private http: HttpClient) {}

  loadConfig(): Promise<void> {
    const salt = (new Date()).getTime();
    return new Promise((resolve, reject) => {
      this.http
        .get<AppConfig>(`/assets/app.config.json?${salt}`)
        .subscribe({
          next: (data) => {
            this.config = data;
            this.loaded = true;
            resolve();
          },
          error(err) {
            reject(err);
          }
        });
    });
  }

  getConfig(): AppConfig {
    if (!this.loaded) {
      throw "Config not loaded yet"
    }
    return this.config;
  }
}
