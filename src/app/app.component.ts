import { Component } from '@angular/core';
import {AppConfigService} from "../service/AppConfigService";
import {environment} from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private appConfigService: AppConfigService) {}

  title = 'angular-demo-app';
  someVar= this.appConfigService.getConfig().appUrl;
  staticVar = environment.apiUrl;
}
