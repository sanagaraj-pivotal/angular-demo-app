#!/usr/bin/env bash
echo "shell dockerusername revision"
kp image save angular-demo-app-$2 \
  --tag $1/angular-demo-app:test \
  --git https://gitlab.com/sanagaraj-pivotal/angular-demo-app.git \
  --git-revision $2 \
  --builder my-builder \
  --service-account spring-demo-service-account \
  --env BP_NODE_RUN_SCRIPTS=build \
  --env BP_WEB_SERVER=nginx \
  --env "BP_WEB_SERVER_ENABLE_PUSH_STATE=true" \
  --env "NODE_ENV=production" \
  --env "BP_WEB_SERVER_ROOT=dist/angular-demo-app" \
  --env "PORT=80"
